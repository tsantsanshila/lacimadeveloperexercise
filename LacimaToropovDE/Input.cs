﻿using LacimaToropovDE.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacimaToropovDE
{


    class Input
    {
        /// <summary>
        /// Reads parameters from console.
        /// </summary> 
        public static InputDE GetParams()
        {

            try
            {
                Console.WriteLine("Please enter number of simulations: ");
                int NumberOfSimulations = ReadInt32(1000000);
                BrownianComputation. Points = new Points[NumberOfSimulations];
                for (int i = 0; i < NumberOfSimulations; i++)
                    BrownianComputation.Points[i] = new Points(365);
                Console.WriteLine("Please enter number of threads: ");
                int NumberOfThreads = ReadInt32(1);

                Console.WriteLine("Please enter number of histogram buckets: ");
                int NumberOfHistogramBuckets = ReadInt32(40);

                Console.WriteLine("Please enter volatility: ");
                double Volatility = ReadDouble(1.0);

                Console.WriteLine("Please enter output file name for histogram of xmax: ");
                string OutputFileNameForHistogramOfXMax = ReadString("xmax.csv", ".csv");

                Console.WriteLine("Please enter output file name for histogram of x(1.0): ");
                string OutputFileNameForHistogramOfX1dot0 = ReadString("x1.csv", ".csv");


                Console.WriteLine("You've completed input: ");
                Console.WriteLine($"Number of simulations: {NumberOfSimulations}");
                Console.WriteLine($"Number of threads: {NumberOfThreads}");
                Console.WriteLine($"Number of histogram buckets: {NumberOfHistogramBuckets}");
                Console.WriteLine($"Volatility: {Volatility}");
                Console.WriteLine($"Output file name for histogram of xmax: {OutputFileNameForHistogramOfXMax}");
                Console.WriteLine($"Output file name for histogram of x(1.0): {OutputFileNameForHistogramOfX1dot0}");

                return new InputDE(NumberOfSimulations, NumberOfThreads, NumberOfHistogramBuckets,
                   Volatility, OutputFileNameForHistogramOfXMax, OutputFileNameForHistogramOfX1dot0, GetFt());
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Unexpected input exception {ex.Message}.");
                return null;
            }
        }
        /// <summary>
        /// Get default parameters for NUnit test
        /// </summary> 
        public static InputDE GetNUnitTestParams()
        {
            return new InputDE(1, 1, 1,  0.0, string.Empty, string.Empty, GetFt());
        }
        /// <summary>
        ///  Use the following piecewise linear forward curve:
        ///  t           𝐹(𝑡)
        ///  0           1
        ///  60/365      2
        ///  150/365     5
        ///  180/365     3
        ///  1.0         5
        /// </summary>
        /// <param name="defaultval">Default value.</param> 
        /// <returns>list of F(t) values</returns>
        static Dictionary<double, double> GetFt( )
        {
            Dictionary<double, double> f = new Dictionary<double, double>();
            f[0.0] = 1.0;
            f[60.0 / 365.0] = 2.0;
            f[150.0 / 365.0] = 5.0;
            f[180.0 / 365.0] = 3.0;
            f[1.0] = 5.0;
            return f;
        }

        /// <summary>
        /// Reads Int32 from console.
        /// </summary>
        /// <param name="defaultval">Default value.</param> 
        /// <returns></returns>
        static int ReadInt32(int defaultval)
        {
            var vconsole = Console.ReadLine();
            int rv = -1;
            var bsuccess = Int32.TryParse(vconsole, out rv);
            if (!bsuccess)
                Console.WriteLine("Error parsing input to integer. We will use a default value for the computation.");
            return bsuccess ? rv : defaultval;
        }
        /// <summary>
        /// Reads string from console.
        /// </summary>
        /// <param name="defaultval">Default value.</param> 
        /// <param name="extension">File extension.</param> 
        static string ReadString(string defaultval, string extension)
        {
            var vconsole = Console.ReadLine();
            return vconsole.Contains(".csv") ? vconsole : defaultval;
        }
        /// <summary>
        /// Reads double from console.
        /// </summary>
        /// <param name="defaultval">Default value.</param> 
        static Double ReadDouble(Double defaultval)
        {
            var vconsole = Console.ReadLine();
            Double rv = -1;
            var bsuccess = Double.TryParse(vconsole, out rv);
            if (!bsuccess)
                Console.WriteLine("Error parsing input to double. We will use a default value for the computation.");
            return bsuccess ? rv : defaultval;
        }
    }
}
