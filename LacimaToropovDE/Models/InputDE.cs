﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacimaToropovDE.Models
{
    class InputDE
    {
        public InputDE(int numberOfSimulations, int numberOfThreads, int numberOfHistogramBuckets,
             double volatility, string outputFileNameForHistogramOfXMax, string outputFileNameForHistogramOfX1dot0, IDictionary<double, double> f)
        {
            NumberOfSimulations = numberOfSimulations;
            NumberOfThreads = numberOfThreads;
            NumberOfHistogramBuckets = numberOfHistogramBuckets;
            Volatility = volatility;
            OutputFileNameForHistogramOfXMax = outputFileNameForHistogramOfXMax;
            OutputFileNameForHistogramOfX1dot0 = outputFileNameForHistogramOfX1dot0;
            F = new SortedDictionary<double,double>(f);
        }
        /// <summary>
        /// Gets/Sets  number of simulations (default: 1,000,000)
        /// </summary>
        public int NumberOfSimulations { get;   set; }
        /// <summary>
        /// Gets/Sets number of threads (default: 1)
        /// </summary>
        public int NumberOfThreads { get; private set; }
        /// <summary>
        /// Gets/Sets number of histogram buckets (default: 40)
        /// </summary>
        public int NumberOfHistogramBuckets { get; private set; }
        /// <summary>
        /// Gets/Sets Volatility (𝜎) (default 1.0)
        /// </summary>
        public double Volatility { get; private set; }
        /// <summary>
        /// Gets/Sets output file name for histogram of 𝑥𝑚𝑎𝑥 (default xmax.csv)
        /// </summary>
        public string OutputFileNameForHistogramOfXMax { get; private set; }
        /// <summary>
        /// Gets/Sets output file name for histogram of 𝑥(1.0) (default x1.csv)
        /// </summary>
        public string OutputFileNameForHistogramOfX1dot0 { get; private set; }

        /// <summary>
        /// Gets/Sets piecewise linear forward curve
        /// </summary>
        public SortedDictionary<double,double> F { get; private set; }

  
    }
}
