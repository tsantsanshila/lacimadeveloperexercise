﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacimaToropovDE.Models
{
    class OutputDE
    {
        public OutputDE(double minimum, double maximum, int frequency,
             double density)
        {
            Minimum = minimum;
            Maximum = maximum;
            Frequency = frequency;
            Density = density; 
        }
        /// <summary>
        /// Gets/Sets minimum value of the interval
        /// </summary>
        public double Minimum { get;   set; }
        /// <summary>
        /// Gets/Sets maximum value of the interval
        /// </summary>
        public double Maximum { get;   set; }
        /// <summary>
        /// Gets/Sets frequency value of the interval
        /// </summary>
        public int Frequency { get;   set; }
        /// <summary>
        /// Gets/Sets density value of the interval
        /// </summary>
        public double Density { get;   set; } 
    }
}
