﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacimaToropovDE.Models
{
    public struct Points 
    {
       
        public Points(int size)
        {
            t = new float[size];
            
            x = new float[size];
#if (DEBUG)
            Price = new float[size];
#endif
        }
        public float[] t { get; set; }
        public float[] x { get; set; }
#if (DEBUG)
        public float[] Price { get; set; }
#endif
    }
}
