﻿using LacimaToropovDE.Models;
 
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LacimaToropovDE
{
    class Histograms
    {
        /// <summary>
        ///  multi-threading lock object
        /// </summary>
        private static object _lock = new object();

        /// <summary>
        /// Forms output
        /// </summary>
        /// <param name="points">list of points (x,t)</param>
        /// <param name="vars">Input parameters</param> 
        public static void FormOutput(IList<Points> points, InputDE vars)
        {
            try
            {
                int maxIndex = 0;
                double max = 0;
                for (int i = 0; i < points.Count; i++)
                {
                    double curMax = points[i].x.Max();
                    if (curMax > max)
                    {
                        max = curMax;
                        var listF = points[i].x.ToList();
                        maxIndex = listF.IndexOf(listF.Max());
                    }
                }
                Task<IList<OutputDE>> xmax = Task.Run(() => FormHistograms(maxIndex, points, vars));
                Task<IList<OutputDE>> x1 = Task.Run(() => FormHistograms(364, points, vars));
                Task.WhenAll(xmax, x1).Wait();
                PrintToCvs(xmax.Result, vars.OutputFileNameForHistogramOfXMax);
                PrintToCvs(x1.Result, vars.OutputFileNameForHistogramOfX1dot0);
            }
            catch (Exception ex)
            { 
                 Console.WriteLine($"Error Forming output: {ex.Message}."); 
            }
        }

        /// <summary>
        /// Form output (we are using comma as a delimiter between columns and dot as decimal separator).
        /// </summary>
        /// <param name="points">list of points (x,t)</param>
        /// <param name="fileName">Input parameters</param> 
        public static void PrintToCvs(IList<OutputDE>  output, string fileName )
        {
            var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)+ "\\" + fileName;
            using (var w = new StreamWriter(path))
            {

                var line = string.Format("Minimum, Maximum, Frequency, Density");
                w.WriteLine(line);
            
                NumberFormatInfo nfi = new NumberFormatInfo();
                nfi.NumberDecimalSeparator = ".";
                for (int i = 0; i < output.Count; i++)
                {  
                    var line2 = string.Format($"{output[i].Minimum.ToString(nfi)},{output[i].Maximum.ToString(nfi)},{output[i].Frequency.ToString(nfi)},{output[i].Density.ToString(nfi)}" ) ;
                    w.WriteLine(line2);
             
                }
                w.Close();
            }
           

        }

        /// <summary>
        /// Form Histogram objects
        /// </summary>
        /// <param name="index">Index of the points to be analyzed in the histogram</param>
        /// <param name="points">points array</param>
        /// <param name="vars">Input parameters</param> 
        /// <return>Returns output vars for the csv file</return>
        private static   IList<OutputDE>  FormHistograms(int index, IList<Points> points, InputDE vars)
        {
            try
            {
                List<double> list = new List<double>();
                List<OutputDE>  bucket = new List<OutputDE>();
                int i = 0;
                lock(_lock)
                {
                    for (i = 0; i < points.Count; i++)
                    {
                        list.Add(points[i].x[index]);
                    }
                    for (i = 0;i< vars.NumberOfHistogramBuckets;i++)
                    {
                        bucket.Add(new OutputDE(0, 0, 0, 0));
                    } 
                }
                List<double> sortedlist = list.OrderBy(k => k).ToList();
                double min = sortedlist[0];
                double max = sortedlist[sortedlist.Count - 1];
                double currentMin = min;
                double step = (max - min) / bucket.Count;
                double currentMax = min + step;

                for (i = 0; i < bucket.Count; i++)
                {
                    if (i != 0)
                    {
                        currentMin += step;
                        currentMax += step;
                    }
                    bucket[i].Minimum = currentMin;
                    bucket[i].Maximum = currentMax;

                }
                i = 0;
                currentMin = min;
                currentMax = min + step;
                int counter = 0;
                int ommittedValueMin = (int) (sortedlist.Count * 0.01);
                int ommittedValueMax = sortedlist.Count - (int)(sortedlist.Count * 0.01);
                for (int j = 0; j < sortedlist.Count; j++)
                {
                    counter++;
                    if (counter< ommittedValueMin || counter> ommittedValueMax) { }
                    else
                        bucket[i].Frequency++;
                    if (sortedlist[j] >= currentMax)
                    {
                        currentMin += step;
                        currentMax += step;
                        i++;
                    }

                }
                double invsumFrequency = 1.0 / sortedlist.Count;// bucket.Sum(p=>p.Frequency);  
                for (i = 0; i < bucket.Count; i++)
                { 
                    bucket[i].Density = bucket[i].Frequency* invsumFrequency; 
                }

                return bucket;
            }
            catch (Exception ex)
            {
                lock (_lock)
                {
                    Console.WriteLine($"Error Forming histogram: {ex.Message}.");
                }
                return null;
            }
        }
    }
}
