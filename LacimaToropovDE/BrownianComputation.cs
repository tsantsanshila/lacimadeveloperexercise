﻿
using LacimaToropovDE.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LacimaToropovDE
{
    class BrownianComputation
    {
        

        /// <summary>
        /// boolean flag to stop the calculation and return already calculated results
        /// </summary>
        private static volatile bool Exit;
         
        /// <summary>
        /// Concurrent dictionary to access Drift Rate precalculated values
        /// </summary>
        private static ConcurrentDictionary<int,double> Drift  { get; set; }

        /// <summary>
        /// Points array where the results of the calculation is stored. No need to handle locking as the threads
        /// never access the values of array with which the other threads are working
        /// </summary>
        public static Points[] Points { get; set; }

        /// <summary>
        /// Get a double random number
        /// </summary>
        /// <return>Random number</return>
        private static double RandomDouble()
        {
            return StaticRandom.RandDouble();
        }

        /// <summary>
        /// Pre-Calculates Drift Rate Concurrent dictionary and time upper boundaries of the intervals
        /// </summary>
        /// <param name="vars">input variables and arrays</param>
        /// <returns>precalculated upper time boundaries of the intervals</returns>
        public static double[] CalculateTimeandDriftRate(InputDE vars)
        {
            if (Drift==null)
            {
                Drift = new ConcurrentDictionary<int, double>();
            }
            else
            {
                Drift.Clear();
            }

            int i = 0;
            double prevX = 0;
            double prevF = 1.0;

            double[] time = new double[vars.F.Count-1];
            foreach (var kp in vars.F)
            {
                if (i == 0)
                {
                    i++;
                    prevX = kp.Key;
                    prevF = kp.Value;
                    continue;
                }
                Drift[i-1] =   (Math.Log(kp.Value) - Math.Log(prevF)) / (kp.Key - prevX)  ;
                time[i - 1] = kp.Key;
                prevX = kp.Key;
                prevF = kp.Value;
                i++;
            }
            return time;



        }
        /// <summary>
        /// Pre-Calculates [ (ln 𝐹(𝑡 +Δ𝑡) − ln 𝐹(𝑡))/Δ𝑡 − 𝜎^2/2] Δ𝑡 values for the drift array
        /// </summary>
        /// <param name="vars">input variables and arrays</param>
        public static double [] PreCalculateDriftSigmaDt( double sigma,int numberOfSamples)
        {
         
            var dt = 1.0 / (numberOfSamples - 1);
            double[] driftSigmaDt = new double[Drift.Count];
            foreach (var kp in Drift)
            {
                driftSigmaDt[kp.Key] = (kp.Value - sigma * sigma * 0.5) * dt; 
            }
            return driftSigmaDt;
        }

        /// <summary>
        /// Get a double random number
        /// </summary>
        /// <param name="vars">Input parameters</param>
        /// <return>Returns list of points (t,F)</return>
        public  static void RunBrownian(InputDE vars )
        {
            try
            {
                // precalculate f(t) array
                var time = CalculateTimeandDriftRate(vars);
                var driftSigmaDt = PreCalculateDriftSigmaDt(vars.Volatility, 365);

               

                int remaining = vars.NumberOfSimulations % vars.NumberOfThreads; // remain simulations are executed in the last thread
                int numsim = vars.NumberOfSimulations / vars.NumberOfThreads;
                 List<Task<int>>  tasks = new  List<Task<int>> ();
                int j = 0;
                int startIndex = 0;
                while (j <  vars.NumberOfThreads )
                { 
                     int numberofsimulationsinthread =  (j == (vars.NumberOfThreads - 1)) ? numsim + remaining : numsim;

                     double[]ds = new double[driftSigmaDt.Length];
                     for (int i = 0; i < driftSigmaDt.Length; i++) ds[i]= driftSigmaDt[i];

                     double[] t = new double[time.Length];
                     for (int i = 0; i < time.Length; i++) t[i] = time[i];


                     int si = startIndex;
                     tasks.Add(Task.Run(() =>   GBMPathList( vars, numberofsimulationsinthread, si, ds, t)));
                     startIndex += numberofsimulationsinthread;
                     j++;
                }
                Task.WhenAll(tasks).Wait() ;
                
            }
            catch (Exception ex)

            {
                Console.WriteLine($"Error Brownian computation: {ex.Message}."); 
            }

        }



        /// <summary>
        /// Generates a  geometric Brownian motion sample paths for the specific number if simulations.
        /// </summary>
        /// <param name="vars">Input parameters</param>
        /// <param name="numsim">Number of simulations in the current thread.</param>
        /// <param name="startIndex">Start index in the Points array</param>
        /// <param name="driftSigmaDt">precalculated [ (ln 𝐹(𝑡 +Δ𝑡) − ln 𝐹(𝑡))/Δ𝑡 − 𝜎^2/2] Δ𝑡 values</param>
        /// <param name="time">upper time boundaries values of the intervals</param>
        /// <returns>Index value of the latest calculated GBMP</returns>
        private static   int  GBMPathList(InputDE vars, int numsim,int startIndex,double[] driftSigmaDt, double[] time)
        {
            int i = startIndex;
            try
            {
                Points[]  results = new  Points[numsim]; 
                double volatility; 
               
                volatility = vars.Volatility; 
               
                int boundary = startIndex + numsim;
                
                for (  ; i < boundary; i++)
                {
                   
                      if (Exit) break;
                      GBMPath(driftSigmaDt, time, 365, 1, volatility,i) ; 
                }
                return i;
            }
            catch (Exception ex)
            {
                 
                 Console.WriteLine($"Error computation geometric Brownian motion sample path: {ex.Message}");
                
                return i;
            }
        }


        /// <summary>
        /// Generates a single geometric Brownian motion sample path.
        /// </summary>
        /// <param name="driftSigmaDt">precalculated [ (ln 𝐹(𝑡 +Δ𝑡) − ln 𝐹(𝑡))/Δ𝑡 − 𝜎^2/2] Δ𝑡 values</param>
        /// <param name="time">upper time boundaries values of the intervals</param>
        /// <param name="numberOfSamples">Number of samples in our case as the frequency is daily and the period is one year Number of Samples equal 365</param>
        /// <param name="initialValue">Initial simulated price value (equals 1 the same as F(0)).</param>
        /// <param name="sigma">Volatility value</param>
        /// <param name="index">Index of the points in the global Points array</param>
        public static  void  GBMPath(  double []driftSigmaDt, double[] time,int numberOfSamples,double initialValue, double sigma,int index)
        {
            try
            {
                var dt = 1.0 / (numberOfSamples - 1); 
                var driftArraySize = driftSigmaDt.Length; 
                Points[index].t[0] = 0;
#if (DEBUG) 
                double Sp =  initialValue; //simulated price
                Points[index].Price[0] =(float)Sp;
#endif
                int j = 0;
                double sigmadt = sigma * Math.Sqrt(dt);
                double x = Math.Log(initialValue);
                Points[index].x[0] = (float)x;
                for (var i = 1; i < numberOfSamples; ++i)
                {
                    float t = (i) * (float)dt;
                    if (t >= time[j] && (j + 1) != driftArraySize) j++;

                    var dx = driftSigmaDt[j] + sigmadt * RandomDouble();
                    
                    x +=  dx;
                    Points[index].t[i] = t;
                    Points[index].x[i] = (float)x;
#if (DEBUG)
                    // here we are using assumption that for the less than 5% price change ln(x) almost equal percentage change of the simulated value
                    Sp *= (float)(1 + dx);
                    Points[index].Price[i] = (float)Sp; 
#endif
                } 
            }
            catch (Exception ex)
            {
             
                  Console.WriteLine($"Error computation geometric Brownian motion sample path: {ex.Message}"); 
            }
        }

        /// <summary>
        /// For the user canceled calculation remove zeros arrays of points
        /// </summary> 
        public static void RemoveZeros( )
        {
            Points = Points.Where(i => i.t[1] > 0.000001).ToArray();   
        }

        /// <summary>
        /// Checks whether computation was canceled by user.
        /// </summary> 
        /// <returns>true if computation is canceled by user</returns>
        public static bool IsCancelledByUserRequest()
        {
            return Exit;
        }
          
        /// <summary>
        /// Sets user canceled computation 
        /// </summary> 
        public static void SetExit( )
        {
          
            Exit = true;

            Console.WriteLine($"Calculation canceled by user's request. Though already calculated results will be printed to the files.");
        }

    }
}
