using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LacimaToropovDE
{
    class Program
    {

        /// <summary>
        ///  
        ///  Lacima Quant Developer Exercise
        ///  
        ///  Objective
        ///  
        ///  Create an application that implements the simulation of the Geometric Brownian
        ///  Motion model of gas prices, and writes out two histograms of the simulated values.
        ///  
        ///  Math
        ///  
        ///  The implementation simulates the logarithm of the incremental differences in price
        ///  using a Geometric Brownian Motion process with a time varying mean:
        ///   Δ𝑥(𝑡) = [ (ln 𝐹(𝑡 +Δ𝑡) − ln 𝐹(𝑡))/Δ𝑡 − 𝜎^2/2] Δ𝑡 + 𝜎√Δ𝑡𝜀(𝑡)
        ///  • 𝑡 is time and runs from 0 (today) to 1.0 (one year from now), in daily
        ///  increments
        ///  • Δ𝑡 is the simulation increment: 1/365
        ///  • 𝑥(𝑡) is the natural log of the simulated price of gas price at time t.
        ///  • Δ𝑥(𝑡) is the simulated 1-day change of the natural logarithm of the gas price
        ///  between t and t + Δ𝑡
        ///  • 𝐹(𝑡) is the forward curve, the expected future gas price.
        ///  • 𝜎 is the annualized volatility(use e.g. 0.2), this is measure for how much the
        ///  price changes
        ///  • 𝜀(𝑡) is a random number from N(0,1)
        ///  For clarity:
        ///  • 𝑥(0) = 𝑙𝑛(𝐹(0))
        ///  • The mean of 𝑥(𝑡) will converge to 𝑙𝑛(𝐹(𝑡))
        ///  • 𝑥(1/365) = 𝑥(0)+Δ𝑥(0)
        ///  • 𝑥(2/365) = 𝑥(1/365)+Δ𝑥(1/365)
        ///  • etc.
        ///  
        ///  It then computes
        ///  • A histogram of the simulated values of 𝑥𝑚𝑎𝑥, which is the maximum
        ///  simulated spot price over the 365 days. Omit the 1% samples at the tails.
        ///  • A histogram of the simulated values of 𝑥(1.0). Omit the 1% samples at the
        ///  tails.
        ///  
        ///  Use the following piecewise linear forward curve:
        ///  t           𝐹(𝑡)
        ///  0           1
        ///  60/365      2
        ///  150/365     5
        ///  180/365     3
        ///  1.0         5
        ///  
        ///  Code
        ///  
        ///  • Implement a console application C# (any version)
        ///  • Implement a unit test to verify that 𝑥(𝑡) will converge to 𝑙𝑛(𝐹(𝑡)).
        ///  
        ///  External libraries
        ///  
        ///  • The core code should not rely on any other nuget packages than
        ///  
        ///  CommandLineParser
        ///  • For the unit test, any nuget libraries available on nuget.org can be used.
        ///  
        ///  Threads
        ///  
        ///  • The application should be multi-threaded.
        ///  • For performance testing, pick a number of simulations that takes about 1
        ///  minute to run in single-threaded mode.
        ///  • Running multiple threads (on multiple cores) should reduce the total run time.
        ///  
        ///  Command Line
        ///  
        ///  The application should be a command line utility accepting these parameters
        ///  • Number of simulations (default: 1,000,000)
        ///  • Number of threads (default: 1)
        ///  • Number of histogram buckets (default: 40)
        ///  • Volatility (𝜎) (default 1.0)
        ///  • Output file name for histogram of 𝑥𝑚𝑎𝑥 (default xmax.csv)
        ///  • Output file name for histogram of 𝑥(1.0) (default x1.csv)
        ///  
        ///  Output
        ///  
        ///  • Output files must be in CSV format with headers
        ///  • Use the columns: Minimum, Maximum, Frequency, Density
        ///  • Output the total number of seconds taken to the console
        ///  
        ///  Progress
        ///  
        ///  • Simulation progress information should be output to the console.
        ///  • Pressing a key should abort the simulations, but still compute the histograms
        ///  using the completed simulations
        ///  
        ///  Guidelines
        ///  
        ///  • Complete coverage by unit tests is not a requirement.
        ///  • Do not implement a GUI of any form.
        ///  • Do not implement more than what we ask for. We are looking for clean,
        ///  efficient and understandable code that performs the described task, not a
        ///  generic framework.
        ///  • Clean and understandable is more important than fast, but within reason.
        ///  
        /// </summary>
        /// <param name="args"></param> 
        static void Main(string[] args)
        {
            var ide = Input.GetParams();
            if (ide == null)
            {
                Console.WriteLine("Press any key to exit...");
                return;
            }
            Console.WriteLine("Please wait, computation is running... (Press ESC to stop calculation)");

            Thread ConsoleKeyListener = new Thread(new ThreadStart(ListerKeyBoardEvent));
            ConsoleKeyListener.Start();
            Stopwatch sw = Stopwatch.StartNew();
           
          

            BrownianComputation .RunBrownian(ide);
          
            sw.Stop();
            Console.WriteLine($"Brownian Computation time taken: {sw.Elapsed.TotalMilliseconds}ms");

            if (BrownianComputation.IsCancelledByUserRequest())
            {
                BrownianComputation.RemoveZeros();
                ide.NumberOfSimulations = BrownianComputation.Points.Length;
            }

            Stopwatch sw2 = Stopwatch.StartNew();
            Console.WriteLine("Compiling the result files...");
            Histograms.FormOutput(BrownianComputation.Points, ide);
            sw2.Stop();
            Console.WriteLine($"Compiling the result files time taken: {sw2.Elapsed.TotalMilliseconds}ms");


         
            Environment.Exit(0); 
        }


        public static void ListerKeyBoardEvent()
        {
              do
               {
                if (Console.ReadKey(true).Key == ConsoleKey.Escape)
                {
                    BrownianComputation.SetExit( );
                }
         
               } while (true);
        }

    }
}
