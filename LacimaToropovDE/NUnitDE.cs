﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacimaToropovDE;
using LacimaToropovDE.Models;
using NUnit.Framework;
namespace LacimaToropovNUnit
{
 
    class NUnitDE
    {
        /// <summary>
        /// A unit test to verify that 𝑥(𝑡) will converge to 𝑙𝑛(𝐹(𝑡)).
        /// </summary> 
        [Test]
        public void BrownianComputationTest()
        {
            var inp = Input.GetNUnitTestParams();
            BrownianComputation.Points = new Points[1];
            BrownianComputation.Points[0] = new Points(365);
            var time = BrownianComputation.CalculateTimeandDriftRate(inp);
            var driftSigmaDt = BrownianComputation.PreCalculateDriftSigmaDt(0, 365);
            BrownianComputation.GBMPath(driftSigmaDt, time,  365, inp.F[0], 0,0);
            var points = BrownianComputation.Points[0];

            Assert.Greater(points.x[59], Math.Log(1.95), "𝑥(𝑡) at t=60/365 doesn't converge to  𝐹(𝑡)");
            Assert.Less(points.x[60], Math.Log(2.01), "𝑥(𝑡) at t=60/365 doesn't converge to  𝐹(𝑡)");

            Assert.Greater(points.x[149], Math.Log(4.85), "𝑥(𝑡) at t=150/365 doesn't converge to  𝐹(𝑡)");
            Assert.Less(points.x[149], Math.Log(5.01), "𝑥(𝑡) at t=150/365 doesn't converge to  𝐹(𝑡)");


            Assert.Greater(points.x[179], Math.Log(2.900),  "𝑥(𝑡) at t=180/365 doesn't converge to  𝐹(𝑡)");
            Assert.Less(points.x[179], Math.Log(3.01), "𝑥(𝑡) at t=180/365 doesn't converge to  𝐹(𝑡)");


            Assert.Greater(points.x[364], Math.Log(4.89), "𝑥(𝑡) at t=1 doesn't converge to  𝐹(𝑡)");
            Assert.Less(points.x[364], Math.Log(5.01), "𝑥(𝑡) at t=1 doesn't converge to  𝐹(𝑡)");

#if (DEBUG)
            Assert.Greater(points.Price[59], 1.95, "Simulated price at t=60/365 doesn't  converge to 𝐹(𝑡)");
            Assert.Less(points.Price[60], 2.01, "Simulated price at t=60/365 doesn't  converge to 𝐹(𝑡)");

            Assert.Greater(points.Price[149], 4.85, "Simulated price at t=150/365 doesn't converge to 𝐹(𝑡)");
            Assert.Less(points.Price[149], 5.01, "Simulated price at t=150/365 doesn't converge to 𝐹(𝑡)");


            Assert.Greater(points.Price[179], 2.900, "Simulated price at t=180/365 doesn't converge to 𝐹(𝑡)");
            Assert.Less(points .Price[179], 3.01, "Simulated price at t=180/365 doesn't converge to 𝐹(𝑡)");


            Assert.Greater(points.Price[364], 4.89, "Simulated price at t=1 doesn't converge to 𝐹(𝑡)");
            Assert.Less(points.Price[364], 5.01, "Simulated price at t=1 doesn't converge to 𝐹(𝑡)");

#endif   
        }
    }
}
